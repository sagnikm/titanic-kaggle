# -*- coding: utf-8 -*-
"""
Created on Sun Mar 11 22:50:17 2018

@author: sagnik
"""
import pandas as pd
import matplotlib.pyplot as plt

train = pd.read_csv('train.csv')

survived = train[train["Survived"] == 1]
died = train[train["Survived"] == 0]

#This code was used to visualize single feature. We found that 
#Pclass, sex and Age has some correlaion with survival

survived["Age"].plot.hist(alpha=0.5,color='red',bins=50)
died["Age"].plot.hist(alpha=0.5,color='blue',bins=50)
plt.legend(['Survived','Died'])

#This is a way to deal with non-numeric feature
sex_pivot = train.pivot_table(index="Sex",values="Survived")
sex_pivot.plot.bar()

male = survived[survived['Sex'] == 'male'].shape #109
female = survived[survived['Sex'] == 'female'].shape #233

ax = survived.plot.scatter(x = "Age", y = "Pclass", color='red')
died.plot.scatter(x = "Age", y = "Pclass", color='blue', ax = ax)
plt.show()
