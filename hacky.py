# -*- coding: utf-8 -*-
"""
Created on Sat Mar 10 17:57:29 2018

@author: sagnik
"""
import pandas as pd
import matplotlib.pyplot as plt
from pandas.plotting import scatter_matrix

test = pd.read_csv('test.csv')
train = pd.read_csv('train.csv')

#train.describe()
#pt.figure()
#train.plot(x = 'PassengerId', y = 'Age')
#scatter_matrix(train, alpha=0.2, figsize=(6, 6), diagonal='kde')

"""
Comparing survivors in different age range.
If age has a good correlation with survival, then
there is a specific age range where there are more
survivors than dead
"""
#survived = train[train["Survived"] == 1]
#died = train[train["Survived"] == 0]
#survived["Age"].plot.hist(alpha=0.5,color='red',bins=50)
#died["Age"].plot.hist(alpha=0.5,color='blue',bins=50)
#plt.legend(['Survived','Died'])
#plt.show()

"""
mostly there are more dead than survived everywhere, but
the discrepancy is more in the range 20 - 30

Comparing survivors in different Pclass range.
"""
#survived["Pclass"].plot.hist(alpha=0.5,color='red',bins=60)
#died["Pclass"].plot.hist(alpha=0.5,color='blue',bins=60)
#plt.legend(['Survived','Died'])
#plt.show()

"""
Almost no correlation
"""

"""
code for training
"""
import numpy as np


train["Age"] = train["Age"].fillna(-0.5)
test["Age"] = test["Age"].fillna(-0.5)

feature_train = np.array(train["Age"].astype(float))
feature_train = feature_train.reshape(-1,1)
feature_test = np.array(test["Age"].astype(float))
feature_test = feature_test.reshape(-1,1)
label_train = np.array(train["Survived"])



from sklearn.naive_bayes import GaussianNB
from sklearn.tree import DecisionTreeClassifier
from sklearn.linear_model import LogisticRegression

clfD = DecisionTreeClassifier(max_features = 1)
clfG = GaussianNB()
clfD.fit(feature_train, label_train)
pred = clfD.predict(feature_test)

pred_surv = feature_test
